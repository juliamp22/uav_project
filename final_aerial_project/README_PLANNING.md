## Path Planner:

There are 3 files in planner node:

### path_planner_node.cpp:

1. Launches a first rotations
2. Go first to the check pad
3. Then go to the shelves area


### path_planner_node:
pose_init_callback -> function that obtains the feedback where the robot is in real time ( needs to be changed: /firefly/vi_sensor/ground_truth/pose)
pose_goal_callback -> function that obtains the goal that the robot should reach
isCollisionFree -> function that check if the robot has a collision on a point
Call_Planning_Path -> function that plans from a start to a destination
MakeRotation -> function that calls the global planning and makes a rotation
Get3dDistance -> obtains the 3D distance between two vectors
Get3dDistance_ -> obtains the 3D distance between two posestamps msgs
PlanandExecute -> calls the planner and executes it
get_m_n -> computes the line between two points
GoToPoint -> tries to go to a point by exploring the unknown map
kjnf