#ifndef COLLISION_CHECK_H
#define COLLISION_CHECK_H

#include <eigen3/Eigen/Eigen>
#include <vector>
#include <map>

#include <ros/ros.h>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Vector3.h"
#include "mav_planning_msgs/PlannerService.h"
#include "final_aerial_project/ProductInfo.h"
#include "std_srvs/Empty.h"
#include "std_msgs/String.h"
#include "std_msgs/Int32.h"
#include "geometry_msgs/Vector3.h"
#include "mav_planning_msgs/PlannerService.h"
#include <Eigen/Dense>
#include <tf2/LinearMath/Quaternion.h>
#include <voxblox_ros/esdf_server.h>
#include <vector>
#include "geometry_msgs/PoseStamped.h"

namespace voxblox
{
    class PathPlanner
    {

    public:
        PathPlanner(const ros::NodeHandle &nh, const ros::NodeHandle &nh_private);

        bool isCollisionFree(const Eigen::Vector3d &position);
        //float tolerance;
        bool publishing;
        //float dist;
        void pose_init_callback(const geometry_msgs::Pose& pose);
        void pose_goal_callback(const geometry_msgs::Pose& pose);
        ros::Subscriber pose_init;
        
        geometry_msgs::Pose firefly_init_pose;
        geometry_msgs::Pose firefly_goal_pose;
        ros::ServiceClient srv_planning_path;
        ros::ServiceClient srv_publish_path;
        bool MakePlanning(const geometry_msgs::PoseStamped& start_pose,const geometry_msgs::PoseStamped& end_pose);
        geometry_msgs::PoseStamped start_pose, goal_pose,waypoint_pose;
        ros::NodeHandle nh_;
        ros::NodeHandle nh_private_;
        voxblox::EsdfServer esdf_server_;
        float tolerance;
        float dist=0;
        bool PlanandExecute(const geometry_msgs::PoseStamped&  start_pose,const geometry_msgs::PoseStamped&  goal_pose);
        float Get3dDistance(std::vector<double>  c1, std::vector<double>  c2);
        float Get3dDistance_(const geometry_msgs::Pose& start_pose,const geometry_msgs::Pose& end_pose );
        std::vector<double> c1,c2;
        float dist_ini;
        bool Call_Planning_Path(const geometry_msgs::PoseStamped& start_pose,const geometry_msgs::PoseStamped& end_pose);
        bool Call_Srv_Publish();
        bool planner_called;
        bool exec_called;
        bool plan_exec;
        bool new_goal;
        void GoToPoint(float z_1);
        std::vector<float> get_m_n(float x_0,float y_0,float x_1,float y_1);
        float Ax;
        float Ay;
        float m;
        float n;
        float k_x; //constante de proporcionalidad x
        float k_y; //constante de proporcionalidad y
        float dist_goal;
        void MakeRotation(float z, float w);
        double distance;
        float x_0;
        float y_0;     
        float x_1;
        float y_1;
        float x_d;
        float y_d;
        float goal_reached;
        bool init_waypoint;
        float dist_waypoint;
        bool is_free;
        ros::Publisher pub_pose;
        std::string item_location;
        void parcel_dispatcher(const final_aerial_project::ProductInfo& parcel_dispacher);
        float GoToPoint_(std::vector<std::vector<float>> point, float z_1);
        geometry_msgs::Pose dispacher_pose;
        void wheretogo_callback(const std_msgs::String& msg);
        void PublishGoal(const geometry_msgs::Pose& pose);
        void pose_init_callback_(const geometry_msgs::PoseStamped& pose);
        void battery_timer_callback(const std_msgs::Int32& msg);
        int remaining_battery;
        void GoToCharge();

        std::string wheretogo;
    protected:
        
        //ros::NodeHandle nh_;
        //ros::NodeHandle nh_private_;
        //voxblox::EsdfServer esdf_server_;
        
        mav_planning_msgs::PlannerService srv_planner;
        
        std_srvs::Empty srv_publisher;
        
        
  

        //parameters
        double p_collision_radius_;

    };
} // namespace voxblox
#endif