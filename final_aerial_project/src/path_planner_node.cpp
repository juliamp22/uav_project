#include "aerial_project/path_planner.h"
#include <Eigen/Dense>

using namespace Eigen;
bool init = true;
bool make_rotation = true;
bool reach_shelve = false;
bool reach_husky = false;
bool reach_dispatcher = false;
bool reach_end = false;
bool gotocharge = false;

std::vector<std::vector<float> > shelve{{7.4,10}, {15,17.4}, {20.1,17.6}, {32.1,17.6},{28,8}, {24.9,5},{23,4.9},{22.2,4.9},{21.82,3.13}};
std::vector<std::vector<float> > husky{{22.2,4.9},{24.9,5}, {29.9,9.9}, {32.1,17.6}, {17.4,22.3},{12.2,26},{9.9,28},{4.8,28},{2.3,28},{2.2,23.26},{2.2,22.8},{5.74,22.8},{5.75,22.82}};
//std::vector<std::vector<float> > charging_dock{{2.2,23.26}, {2.3,28}, {9.9,28}, {12.2,26},{15,17.4},{7.4,10},{3.5,3.5}};
std::vector<std::vector<float> > end_{{22.22,4.9},{22.2,7.2},{24.8,8.4},{29.9,9.9},{32.4,12.5}, {32.1,17.6},{15,17.4},{3.5,3.5}};//{21.82,3.13},{22.2,4.9}, {23,4.9}, {24.9,5}, {28,8}, {32.1,17.6}, {20.1,17.6}, {15,17.4}, {7.4,10},{3.5,3.5}};
std::vector<std::vector<float> > dispatcher{{5.88,23},{2.2,22.8},{2.2,23.26}, {2.3,28}, {9.9,28}, {12.2,26},{15,17.4}, {20.1,17.6}, {32.1,17.6},{28,8}, {24.9,5},{23,4.9},{22.2,4.9},{21.82,3.13}};


int main(int argc, char **argv)
{
    ros::init(argc, argv, "path_planner_node");
    ros::NodeHandle nh("");
    ros::NodeHandle nh_private("~");
    FLAGS_alsologtostderr = true;


    ROS_INFO("Running path planner node...");
    voxblox::PathPlanner planner_node(nh, nh_private);

    //Init params
    nh.setParam("/firefly/voxblox_node/clear_sphere_for_planning", false);
    nh.setParam("/firefly/voxblox_node/clear_sphere_radius", 10);
    nh.setParam("/firefly/voxblox_node/occupied_sphere_radius", 0);

    //Vector3d v(-3,-3,2.5);
    //bool is_free =planner_node.isCollisionFree(v) ;
    std::cout << "holis" << std::endl;

    ros::Subscriber sub_init = nh.subscribe("/firefly_filtered/pose",10,&voxblox::PathPlanner::pose_init_callback_, &planner_node);
    ros::Subscriber sub_goal = nh.subscribe("/firefly/goal",10,&voxblox::PathPlanner::pose_goal_callback, &planner_node);
    ros::Subscriber sub_dispatcher = nh.subscribe("/parcel_dispatcher/next_product",10,&voxblox::PathPlanner::parcel_dispatcher, &planner_node);
    ros::Subscriber wheretogo_ = nh.subscribe("/mission/wheretogo",10,&voxblox::PathPlanner::wheretogo_callback, &planner_node);
    ros::Subscriber battery_timer = nh.subscribe("/firefly/battery_timer",10,&voxblox::PathPlanner::battery_timer_callback, &planner_node);
    ros::Rate r(10);
    ROS_INFO("Running mission node...");
    while(ros::ok()){
      ros::spinOnce();
      //bool is_free =planner_node.isCollisionFree(v) ;      
      planner_node.start_pose.header.frame_id = "world";
      planner_node.start_pose.pose.position = planner_node.firefly_init_pose.position;
      planner_node.start_pose.pose.orientation = planner_node.firefly_init_pose.orientation;
      planner_node.goal_pose.header.frame_id = "world";
      planner_node.goal_pose.pose.position.x = planner_node.firefly_goal_pose.position.x;
      planner_node.goal_pose.pose.position.y = planner_node.firefly_goal_pose.position.y;
      planner_node.goal_pose.pose.position.z = planner_node.firefly_goal_pose.position.z;
      //std::cout << "new_goal" <<  planner_node.new_goal << std::endl;
      //std::cout << "init" <<  init << std::endl;
      //std::cout << planner_node.firefly_goal_pose << std::endl;
      //std::cout << init << std::endl;
      if ((planner_node.firefly_init_pose.position.x != 0)or(planner_node.firefly_init_pose.position.y != 0)or(planner_node.firefly_init_pose.position.z != 0)){
        //std::cout << "holis" << std::endl;
        //planner_node.x_1 = planner_node.firefly_init_pose.position.x;
      if (make_rotation){
          planner_node.MakeRotation(0.737585179684,0.675254102327);
          std::cout << "make_rotation" << make_rotation << std::endl;
          init = false;
          make_rotation = false;
          ros::Duration(5).sleep();

        }
      
      std::cout <<  "gotocharger" << gotocharge << std::endl;
      if (( planner_node.wheretogo ==  "dispatcher")&&(not gotocharge)){
        if (not reach_shelve){
            try{
               std::cout << "reaching dispatcher" << std::endl;
              float dist = planner_node.GoToPoint_(shelve,1);
              //std::cout << "dist" << dist << std::endl;
              if ((dist<3)and(shelve.size()>1)){
                  shelve.erase(shelve.begin());
              }
              if ((shelve.size()==1)&&(dist<2)){
                reach_shelve = true;
                std::cout << planner_node.dispacher_pose << std::endl;
                //planner_node.PublishGoal(planner_node.dispacher_pose);
                std::cout << "finished" << std::endl;
              }
            }
            catch(...){
              std::cout << "robot can't reach the position" << std::endl;
            }
          }
      }
      if (( planner_node.wheretogo == "husky")&&(not gotocharge)){
        if (not reach_husky){
          //planner_node.item_location == "HUSKY";
          //if (planner_node.item_location == "HUSKY"){
            try{
              std::cout << "reaching husky" << std::endl;
              std::cout << "husky" << husky[0][0] << std::endl;
              float dist = planner_node.GoToPoint_(husky,1);
              std::cout << "dist" << dist << std::endl;
              if ((dist<3)and(husky.size()>1)){
                  husky.erase(husky.begin());
              }
              if ((husky.size()==1)&&(dist<1.5)){
                reach_husky = true;
                std::cout << planner_node.dispacher_pose << std::endl;
                //planner_node.PublishGoal(planner_node.dispacher_pose);
                std::cout << "finished" << std::endl;
              }
            }
            catch(...){
              std::cout << "robot can't reach the position" << std::endl;
            }
          
        }
      }
      else if (( planner_node.wheretogo == "dispatcher_")&&(not gotocharge)){
         if (not reach_dispatcher){
            try{
               std::cout << "reaching dispatcher" << std::endl;
              float dist = planner_node.GoToPoint_(dispatcher,1);
              std::cout << "dist" << dist << std::endl;
              if ((dist<3)and(dispatcher.size()>1)){
                  dispatcher.erase(dispatcher.begin());
              }
              if ((dispatcher.size()==1)&&(dist<2)){
                reach_dispatcher = true;
                std::cout << planner_node.dispacher_pose << std::endl;
                //planner_node.PublishGoal(planner_node.dispacher_pose);
                std::cout << "finished" << std::endl;
              }
            }
            catch(...){
              std::cout << "robot can't reach the position" << std::endl;
            }
          }
      }

        else if (( planner_node.wheretogo == "end")&&(not gotocharge)){
        
              if (not reach_end){
                  std::cout << "reaching end" << std::endl;
                  try{
                
                    float dist = planner_node.GoToPoint_(end_,1);
                    //std::cout << "dist" << dist << std::endl;
                    if ((dist<3)and(end_.size()>1)){
                        end_.erase(end_.begin());
                    }
                    if ((end_.size()==1)&&(dist<2)){
                      reach_end = true;
                      //std::cout << planner_node.dispacher_pose << std::endl;
                      //planner_node.PublishGoal(planner_node.dispacher_pose);
                      std::cout << "finished" << std::endl;
                    }
                  }
            catch(...){
              std::cout << "robot can't reach the position" << std::endl;
            }
          }
           }

     

      }
      

      r.sleep();
    }
      return 0;

}