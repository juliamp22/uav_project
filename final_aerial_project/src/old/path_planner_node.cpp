#include "aerial_project/path_planner.h"
#include <Eigen/Dense>

using namespace Eigen;

void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  ROS_INFO("I heard: [%s]", msg->data.c_str());
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "path_planner_node");
    ros::NodeHandle nh("");
    ros::NodeHandle nh_private("~");
    FLAGS_alsologtostderr = true;

    ROS_INFO("Running path planner node...");
    voxblox::PathPlanner planner_node(nh, nh_private);

    Vector3d v(-3,-3,2.5);
    bool is_free =planner_node.isCollisionFree(v) ;
    ROS_INFO(is_free);

    ros::Subscriber sub = n.subscribe("chatter", 1000, chatterCallback);

    ros::spin();
    
    return 0;
}