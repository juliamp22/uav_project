
#include <ros/ros.h>
#include "geometry_msgs/PointStamped.h"
#include "geometry_msgs/Transform.h"
#include "geometry_msgs/Vector3.h"
#include <tf/transform_datatypes.h>

#include <std_msgs/String.h>
#include <nav_msgs/Odometry.h>

#include <tf/transform_listener.h>

using namespace std;

ros::Publisher * pose_world_pub;

void filtered_callback(nav_msgs::Odometry pose){

	geometry_msgs::PoseWithCovariance firefly_filtered = pose.pose;


//world_T_drone = world_T_odom*odom_T_drone

  tf::Transform world_T_odom;
  world_T_odom.setOrigin(tf::Vector3(3.5,3.5,0.15));
  world_T_odom.setRotation(tf::Quaternion(0,0,0,1));

  tf::Transform odom_T_drone;
  odom_T_drone.setOrigin(tf::Vector3(firefly_filtered.pose.position.x,firefly_filtered.pose.position.y,firefly_filtered.pose.position.z));
  odom_T_drone.setRotation(tf::Quaternion(firefly_filtered.pose.orientation.x, firefly_filtered.pose.orientation.y, firefly_filtered.pose.orientation.z, firefly_filtered.pose.orientation.w));

  tf::Transform world_T_drone;
  world_T_drone = world_T_odom*odom_T_drone;

  geometry_msgs::PoseStamped drone_pose;
  drone_pose.pose.position.x = world_T_drone.getOrigin().x();
  drone_pose.pose.position.y = world_T_drone.getOrigin().y();
  drone_pose.pose.position.z = world_T_drone.getOrigin().z();
  drone_pose.pose.orientation.x =  world_T_drone.getRotation().x();
  drone_pose.pose.orientation.y =  world_T_drone.getRotation().y();
  drone_pose.pose.orientation.z =  world_T_drone.getRotation().z();
  drone_pose.pose.orientation.w =  world_T_drone.getRotation().w();
  drone_pose.header.frame_id = "world";
  drone_pose.header.stamp = ros::Time::now();

  pose_world_pub -> publish(drone_pose);
}


int main(int argc, char ** argv) {

    ros::init(argc, argv, "odom to world");
    ros::NodeHandle nh;
   
    ros::Subscriber sub_transf = nh.subscribe("/firefly_filtered/odom", 1, filtered_callback);
    pose_world_pub = new ros::Publisher(nh.advertise<geometry_msgs::PoseStamped>("/firefly_filtered/pose", 1));

    ros::spin();

    return 0;
}
