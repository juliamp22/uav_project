#include <ros/ros.h>
#include "geometry_msgs/PoseStamped.h"
#include "final_aerial_project/ProductInfo.h"
#include "final_aerial_project/ProductFeedback.h"
#include "mav_planning_msgs/PlannerService.h"
#include "std_srvs/Empty.h"
#include "chrono"
#include "std_msgs/String.h"

geometry_msgs::Pose firefly_pose;
geometry_msgs::Pose dispatcher_pose;
final_aerial_project::ProductInfo request_product;
std::string target_product = "";

ros::ServiceClient srv_planning_path;
mav_planning_msgs::PlannerService srv_planner;

ros::ServiceClient srv_publish_path;
std_srvs::Empty srv_publisher;
ros::Publisher * feedback_pose_pub;
ros::Publisher * goal_pose_pub;
ros::Publisher * path_goal_pub;

bool publishing = false;
bool feedback = false;
bool aprox = false;
geometry_msgs::PoseStamped goal;

geometry_msgs::PoseStamped fiducial_pose;
final_aerial_project::ProductFeedback pose_marker;

double deltax, deltay, deltaz;
geometry_msgs::PoseStamped pose_temp;


float Get3dDistance( geometry_msgs::Pose firefly_pose, geometry_msgs::Pose goal_pose )
{
	std::vector<double> c1,c2;
	c1 = {goal_pose.position.x, goal_pose.position.y, goal_pose.position.z};
    c2 = {firefly_pose.position.x, firefly_pose.position.y, firefly_pose.position.z};
   
    float dx = c2[0] - c1[0];
    float dy = c2[1] - c1[1];
    float dz = c2[2] - c1[2];

return sqrt((float)(dx * dx + dy * dy + dz * dz));
}

bool checkTwoPointEquals(geometry_msgs::PoseStamped pose_now){

	double dist;
	dist = Get3dDistance(pose_now.pose, pose_temp.pose);

	if (dist > 0.2){
		pose_temp = pose_now;
		ROS_INFO("NO iguals");
		//return false;
		return true;
	}
	else{
		ROS_INFO("Dos d'iguals");
		return true;

	}

}

void goToDispatcher(){


	dispatcher_pose.position.x = 21.8;
	dispatcher_pose.position.y = 3.0;
	dispatcher_pose.position.z = 0.8;


/*	
		goal.pose.position.x = 21.8;
		goal.pose.position.y = 3.0;
		goal.pose.position.z = 0.8; 
		goal.pose.orientation.x = 1;//0.01927;
		goal.pose.orientation.y = -1;//0.01403;
		goal.pose.orientation.z = 0;
		goal.pose.orientation.w = 0;//-0.67669;
		goal.header.frame_id = "world";


					goal_pose_pub -> publish(goal);
		publishing = true;
		

*/
   	srv_planner.request.start_pose.pose = firefly_pose;
    srv_planner.request.goal_pose.pose = dispatcher_pose;
    srv_planner.request.bounding_box.x = 10;
    srv_planner.request.bounding_box.y = 10;
    srv_planner.request.bounding_box.z = 10;

    try{
        srv_planning_path.call(srv_planner);
    }
    catch(...){}


//Miramos a ver si planner success= true con 5 segundos de timeout
    auto start = std::chrono::system_clock::now();
    auto end = std::chrono::system_clock::now();
    while((srv_planner.response.success == false)and(std::chrono::duration_cast<std::chrono::seconds>(end - start).count() != 5)){

        end = std::chrono::system_clock::now();   
    }
  

   if (srv_planner.response.success == false)
    {  
        ROS_ERROR("Ha fallado al planear");
     } 
    else{
    
    //si se ha podido planear hacemos publish de los puntos. 
        srv_publish_path.call(srv_publisher);
        publishing = true;

       }
}

final_aerial_project::ProductFeedback searchMarkerPosition(geometry_msgs::Pose aprox_pose, int id_marker){


if (publishing == true){
	float dist = Get3dDistance( firefly_pose, goal.pose);
	if(dist<0.4){
		publishing = false;
		ROS_INFO("S'ha arribat al punt");
	}
}
else{
	
		goal.pose.position.x = aprox_pose.position.x + deltax;
		goal.pose.position.y = aprox_pose.position.y ;
		goal.pose.position.z = aprox_pose.position.z + deltaz;
		goal.pose.orientation.x = 0;//0.01927;
		goal.pose.orientation.y = 0;//0.01403;
		goal.pose.orientation.z = -1;
		goal.pose.orientation.w = 1;//-0.67669;
		goal.header.frame_id = "world";
		

		if (deltax > -2.0){
		deltax = deltax - 0.6;}
		else{
			deltax = + 2.0;
		}
		if (deltaz < 5){
		deltaz = deltaz + 0.5;}
		else{
			deltaz = 1.5;
		}
		
		goal_pose_pub -> publish(goal);
		publishing = true;

		std::cout << "ID_marker" << id_marker << "Frame id: "<< fiducial_pose.header.frame_id << std::endl;
		if ((fiducial_pose.header.frame_id == std::to_string(id_marker))and fiducial_pose.pose.position.x >0.1)
		{	
			bool equal = false;
			equal = checkTwoPointEquals(fiducial_pose);
			if (equal == true){
			final_aerial_project::ProductFeedback temp;
			temp.approximate_position = fiducial_pose.pose.position;
			temp.marker_id = id_marker;
			feedback = true;
			publishing= false;
			deltax = 0;
			deltaz = 0;
			fiducial_pose.pose.position.x = 0;
			return temp;}
		}
    }
}


final_aerial_project::ProductFeedback searchHuskyPosition(geometry_msgs::Pose aprox_pose, int id_marker){


if (publishing == true){
	float dist = Get3dDistance( firefly_pose, goal.pose);
	if(dist<0.4){
		publishing = false;
		ROS_INFO("S'ha arribat al punt");
	}
}
else{
	
		goal.pose.position.x = aprox_pose.position.x + deltax;
		goal.pose.position.y = aprox_pose.position.y + deltay;
		goal.pose.position.z = 1.2;//aprox_pose.position.z;
		goal.pose.orientation.x = 0.5;//0.01927;
		goal.pose.orientation.y = 0.0;
		goal.pose.orientation.z = 0.0;
		goal.pose.orientation.w = 0.82;
		goal.header.frame_id = "world";
		

		if (deltax < 1.0){
		deltax = deltax +0.3;}
		else{
			deltax = 0;
		}
		if (deltay < 2.5){
		deltay = deltay + 0.4;}
		else{
			deltay = 0;
		}
		
		goal_pose_pub -> publish(goal);
		ros::Duration(1.0).sleep();
		goal_pose_pub -> publish(goal);

		publishing = true;

	//	std::cout << "ID_marker" << id_marker << "Frame id: "<< fiducial_pose.header.frame_id << std::endl;
		if (fiducial_pose.pose.position.x >0.1)
		{	
			bool equal = false;
			equal = checkTwoPointEquals(fiducial_pose);
			if (equal == true){
			final_aerial_project::ProductFeedback temp;
			temp.approximate_position = fiducial_pose.pose.position;
			temp.marker_id = id_marker;
			feedback = true;
			publishing= false;
			deltax = 0;
			deltaz = 0;
			fiducial_pose.pose.position.x = 0;
			return temp;}
		}
    }
}

void mission(){

//target_product = "HUSKY";
	
if(target_product == "")
	{
		geometry_msgs::Pose dispatcher_pose;
	    dispatcher_pose.position.x = 21.8;
	    dispatcher_pose.position.y = 3.0;
	    dispatcher_pose.position.z = 1.0;
//Llegar a dispatcher station
//XXXXXX
	    std_msgs::String goal_text;
	    goal_text.data = "dispatcher";
   		path_goal_pub -> publish(goal_text);


		float dist = Get3dDistance(firefly_pose, dispatcher_pose);
		if (abs(dist)<0.8){

		ROS_INFO(" Estoy en parcel dispatcher !!");
		target_product = request_product.item_location;

		}

	}
else if(target_product == "SHELVE")
	{	ROS_INFO("Shelving...............");
		std::cout << "fd:" << feedback <<  "publishing:" << publishing << std::endl;

		int id_marker = request_product.marker_id;

		if(feedback == false){

		/*	geometry_msgs::Pose goal_ini;
			goal_ini.position.x = request_product.approximate_pose.pose.position.x;
			goal_ini.position.y = request_product.approximate_pose.pose.position.y;
			goal_ini.position.z = request_product.approximate_pose.pose.position.z;
			
			srv_planner.request.start_pose.pose = firefly_pose;
		    srv_planner.request.goal_pose.pose = goal_ini;
		    srv_planner.request.bounding_box.x = 10;
		    srv_planner.request.bounding_box.y = 10;
		    srv_planner.request.bounding_box.z = 10;

		   	auto start = std::chrono::system_clock::now();
		    auto end = std::chrono::system_clock::now();
		    while((srv_planner.response.success == false)and(std::chrono::duration_cast<std::chrono::seconds>(end - start).count() != 5)){
		        end = std::chrono::system_clock::now();   
		    }
			if (srv_planner.response.success == false)
			    { ROS_ERROR("Ha fallado al planear");   } 
			else{   
		    //si se ha podido planear hacemos publish de los puntos. 
		        srv_publish_path.call(srv_publisher);
		        publishing = true;
		        }

			float dist = Get3dDistance(firefly_pose, goal_ini);
			std::cout << "dist" << dist << std::endl;
			if ( abs(dist) < 0.5 and dist < 0){*/
				pose_marker = searchMarkerPosition(request_product.approximate_pose.pose, id_marker);
			//	}

		}
		else{
			if(publishing == false){// and(pose_marker.approximate_position.x > 0.1)){
			std::cout << "Marker position: !!! " << pose_marker << std::endl;
			goToDispatcher();
			}
			else{  // if (publishing == true){
				float dist = Get3dDistance( firefly_pose, dispatcher_pose);
				if(abs(dist)<0.3){
					publishing = false;
					ROS_INFO(" Estoy en parcel dispatcher !!");
					feedback_pose_pub -> publish(pose_marker);
				    //pose_marker.approximate_position.x = 0;
				    //fiducial_pose.pose.position.x = 0;
				    feedback = false;
				    //id_marker= id_marker +1;
				    ros::Duration(1.0).sleep();
		    		target_product = request_product.item_location;


				}	
			}
		}
	}
else if(target_product == "HUSKY")
	{
		ROS_INFO("Huskying...............");
		std::cout << "fd:" << feedback <<  "publishing:" << publishing << std::endl;

		int id_marker = request_product.marker_id;
		//std::cout << "Request product: " << request_product << std::endl;
   			    std_msgs::String goal_text2;
   			    goal_text2.data = "husky";

   		path_goal_pub -> publish(goal_text2);

		float dist = Get3dDistance(firefly_pose, dispatcher_pose);
		if (abs(dist)<0.8){
			aprox = true;
		}
		if((feedback == false) and (aprox == true)){
		/*geometry_msgs::Pose husky_aprox_pose;
		husky_aprox_pose.position.x = 5.75;
		husky_aprox_pose.position.y = 22.8;
		husky_aprox_pose.position.z= 1.0;*/


		//pose_marker = searchHuskyPosition(husky_aprox_pose, id_marker);
		pose_marker = searchHuskyPosition(request_product.approximate_pose.pose, id_marker);
		}
		else{
			//if(publishing == false){// and(pose_marker.approximate_position.x > 0.1)){
			std::cout << "Husky position: !!! " << pose_marker << std::endl;
			goToDispatcher();
			//}
			//else{  // if (publishing == true){
				float dist = Get3dDistance( firefly_pose, dispatcher_pose);
				if(abs(dist)<0.3){
					publishing = false;
					ROS_INFO(" Estoy en parcel dispatcher !!");
					feedback_pose_pub -> publish(pose_marker);
				    //pose_marker.approximate_position.x = 0;
				    //fiducial_pose.pose.position.x = 0;
				    feedback = false;
				    //id_marker= id_marker +1;
				    ros::Duration(1.0).sleep();
				    target_product = request_product.item_location;


				//}	
			}
		}
	}
else if(target_product == "END")
	{

	}


}

void product_request_callback(final_aerial_project::ProductInfo product){

	request_product=product;

}

void firefly_pose_callback(geometry_msgs::PoseStamped pose){

	firefly_pose = pose.pose;
}

void fiducial_marker_callback(geometry_msgs::PoseStamped pose){

	fiducial_pose = pose;
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "mission_node");
    ros::NodeHandle nh("");

	ros::Subscriber sub_posedrone = nh.subscribe ("/firefly/vi_sensor/ground_truth/pose", 1, firefly_pose_callback);
    ros::Subscriber sub_productrequest = nh.subscribe ("/parcel_dispatcher/next_product", 1, product_request_callback);
    ros::Subscriber sub_fiducial_marker = nh.subscribe ("/firefly/vi_sensor/camera_depth/estimated_marker_point", 1, fiducial_marker_callback);

	ros::service::waitForService("/firefly/voxblox_rrt_planner/plan", -1);
    ros::service::waitForService("/firefly/voxblox_rrt_planner/publish_path", -1);

    srv_planning_path = nh.serviceClient<mav_planning_msgs::PlannerService>("/firefly/voxblox_rrt_planner/plan");
    srv_publish_path = nh.serviceClient<std_srvs::Empty>("/firefly/voxblox_rrt_planner/publish_path");

    feedback_pose_pub = new ros::Publisher(nh.advertise<final_aerial_project::ProductFeedback>("/firefly/product_feedback", 1));

	goal_pose_pub = new ros::Publisher(nh.advertise<geometry_msgs::PoseStamped>("/firefly/command/pose", 1));

	path_goal_pub = new ros::Publisher(nh.advertise<std_msgs::String>("/mission/wheretogo", 1));

    ROS_INFO("Running mission node...");

    mission();

        ros::Rate r(5);
    while(ros::ok()){
        ros::spinOnce();
        mission();  
        r.sleep();
        }


    return 0;
}