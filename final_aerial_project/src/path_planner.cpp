#include <ros/ros.h>
#include "aerial_project/path_planner.h"


namespace voxblox
{

    PathPlanner::PathPlanner(const ros::NodeHandle &nh, const ros::NodeHandle &nh_private)
        : nh_(nh),
          nh_private_(nh_private),
          esdf_server_(nh_, nh_private_)
    {
        srv_planning_path = nh_.serviceClient<mav_planning_msgs::PlannerService>("/firefly/voxblox_rrt_planner/plan");
        srv_publish_path = nh_.serviceClient<std_srvs::Empty>("/firefly/voxblox_rrt_planner/publish_path");
        geometry_msgs::Pose pose1;
        firefly_init_pose = pose1;
        planner_called = false;
        publishing = false;
        tolerance = 0.8;
        plan_exec = true;   
        k_x = 0.01;
        k_y = 0.05;
        p_collision_radius_ = 0.6;
        x_0 = 0;
        y_0 =0;
        x_1 = 0;
        y_1 = 0;
        goal_reached = false;
        init_waypoint = true;
        dist_waypoint = 0.0;
        pub_pose = nh_.advertise<geometry_msgs::PoseStamped>("/firefly/command/pose", 1000);
        

        
    }

    void PathPlanner::pose_init_callback_(const geometry_msgs::PoseStamped& pose)
    {
        geometry_msgs::PoseStamped pose1 = pose;
        firefly_init_pose = pose1.pose;
        //std::cout << "Pose_init" << firefly_init_pose << std::endl;
    }

    void PathPlanner::pose_init_callback(const geometry_msgs::Pose& pose)
    {
        geometry_msgs::Pose pose1 = pose;
        firefly_init_pose = pose1;
           //std::cout << "Pose_init" << pose << std::endl;
    }

    void PathPlanner::pose_goal_callback(const geometry_msgs::Pose& pose)
    {
        geometry_msgs::Pose pose2 = pose;
        firefly_goal_pose = pose2;
        new_goal = true;
        std::cout << "new_goal" << new_goal << std::endl;
    }

    void PathPlanner::parcel_dispatcher(const final_aerial_project::ProductInfo& parcel_dispacher)
    {
        item_location = parcel_dispacher.item_location;
        //std::cout << "parcel_dispacher" << parcel_dispacher << std::endl;
        dispacher_pose = parcel_dispacher.approximate_pose.pose;
        /*geometry_msgs::Pose pose2 = pose;
        firefly_goal_pose = pose2;
        new_goal = true;
        std::cout << "new_goal" << new_goal << std::endl;*/
    }
    void PathPlanner::wheretogo_callback(const std_msgs::String& msg)
    {
        std::string data = msg.data;
        wheretogo = data;
        std::cout << "msg" << data;
        std::cout << "wheretogo_" << wheretogo;
        /*geometry_msgs::Pose pose2 = pose;
        firefly_goal_pose = pose2;
        new_goal = true;
        std::cout << "new_goal" << new_goal << std::endl;*/
    }

       void PathPlanner::battery_timer_callback(const std_msgs::Int32& msg)
    {
        std::cout << "msg" << msg << std::endl;
        remaining_battery = msg.data;
        /*geometry_msgs::Pose pose2 = pose;
        firefly_goal_pose = pose2;
        new_goal = true;
        std::cout << "new_goal" << new_goal << std::endl;*/
    }

    

    // Method that checks whether a point is occupied or not 
    // The point is collision free if it's distance to the nearest obstacle is bigger
    // than the collision radius defined


    bool PathPlanner::isCollisionFree(const Eigen::Vector3d &position)
    {
        distance = 0.0;
        const bool kInterpolate = false;
        esdf_server_.getEsdfMapPtr()->getDistanceAtPosition(
            position, kInterpolate, &distance);
        //td::cout << "distance__" << distance << std::endl;
        //std::cout << "p_collision_Radius" << p_collision_radius_ << std::endl;
        if (distance < p_collision_radius_)
        {
            //std::cout << "distance" << distance << std::endl;
            //std::cout << "p_collision_Radius" << p_collision_radius_ << std::endl;
            return false;
        }
        return true;
    }


    bool PathPlanner::Call_Planning_Path(const geometry_msgs::PoseStamped& start_pose_,const geometry_msgs::PoseStamped& end_pose_){
        srv_planner.request.start_pose = start_pose_;
        srv_planner.request.goal_pose = end_pose_;
        srv_planner.request.bounding_box.x = 20;
        srv_planner.request.bounding_box.y = 20;
        srv_planner.request.bounding_box.z = 20;
        //std::cout << "start_pose_" << start_pose_ << std::endl;
        //std::cout << "end_pose_" << end_pose_ << std::endl;
        //std::cout << "Pose_init" << start_pose << std::endl;
        //std::cout << "Pose_goal" << goal_pose << std::endl;
        try{
            srv_planning_path.call(srv_planner);
            //Miramos a ver si planner success= true con 1 segundos de timeout
            auto start = std::chrono::system_clock::now();
            auto end = std::chrono::system_clock::now();
            while((srv_planner.response.success == false)and(std::chrono::duration_cast<std::chrono::seconds>(end - start).count() != 1)){
                end = std::chrono::system_clock::now();   
            }
            if (srv_planner.response.success){
                srv_publish_path.call(srv_publisher); 
                publishing = true;
                return true;
            }
            else{
               // publishing = false;
                return false;
            }
        }
        catch(...){
                publishing = false;
              return false;
        }
    }

    bool PathPlanner::Call_Srv_Publish(){
        try{
            srv_publish_path.call(srv_publisher); 
            return true;
        }
        catch(...){
            return false;
        }
    }

    void PathPlanner::GoToCharge(){
        start_pose.header.frame_id = "world";
        start_pose.pose.position = firefly_init_pose.position;
        start_pose.pose.orientation = firefly_init_pose.orientation;
        goal_pose.header.frame_id = "world";
        goal_pose.pose.position.x = 3.5;
        goal_pose.pose.position.y = 3.5;
        goal_pose.pose.position.z = 1;  
          goal_pose.pose.orientation.w = 1;
        Call_Planning_Path(start_pose,goal_pose);



    }
    void PathPlanner::MakeRotation(float z, float w){
            geometry_msgs::PoseStamped pose_;
            pose_.header.frame_id = "world";
            pose_.pose.position = firefly_init_pose.position;
            pose_.pose.position.z = 1.5;
            pose_.pose.orientation.z = z;
            pose_.pose.orientation.w = w;
            start_pose.pose = firefly_init_pose;
            pub_pose.publish(pose_);


            //std::cout << "pose" << pose_ << std::endl;
            //std::cout << "imit_pose" << start_pose << std::endl;
            //bool call_pl = Call_Planning_Path(start_pose,pose_);
            //return call_pl;
    }





    /*bool PathPlanner::MakePlanning(const geometry_msgs::PoseStamped& start_pose_,const geometry_msgs::PoseStamped& end_pose_){
        srv_planner.request.start_pose = start_pose_;
        srv_planner.request.goal_pose = end_pose_;
        srv_planner.request.bounding_box.x = 10;
        srv_planner.request.bounding_box.y = 10;
        srv_planner.request.bounding_box.z = 10;
        //std::cout << "hostd::cout << "p_collision_Radius" << p_collision_radius_ << std::endl;
            return false;se2" << firefly_init_pose << std::endl;
        std::cout << "Pose_init" << start_pose << std::endl;
        std::cout << "Pose_goal" << goal_pose << std::endl;
        try{
            srv_planning_path.call(srv_planner);
            std::cout << "started to plan" << std::endl;
        }
        catch(...){
              std::cout << "not started to plan" << std::endl;
        }

        auto start = std::chrono::system_clock::now();
        auto end = std::chrono::system_clock::now();

       /* while((srv_planner.response.success == false)&(std::chrono::duration_cast<std::chrono::seconds>(end - start).count() != 5)){

            end = std::chrono::system_clock::now();   
        }

        if (srv_planner.response.success == false){  
            ROS_ERROR("Ha fallado al planear");
            return false;
        } 
        else{
            //si se ha podido planear hacemos publish de los puntos. 
            try{
                srv_publish_path.call(srv_publisher);  
                publishing = true;
                return true;
                ROS_ERROR("La ejecucion ha funcionado");
                }
            catch(...){
                publishing = false;
                 return false;
                 ROS_ERROR("Ha fallado la ejecucion");
            }
        }
    return false;
    }*/

    float PathPlanner::Get3dDistance( std::vector<double>  c1, std::vector<double>  c2 )
        {
            float dx = c2[0] - c1[0];
            float dy = c2[1] - c1[1];
            float dz = c2[2] - c1[2];

        return sqrt((float)(dx * dx + dy * dy + dz * dz));
        }

    float PathPlanner::Get3dDistance_(const geometry_msgs::Pose& start_pose_,const geometry_msgs::Pose& end_pose_ )
        {
            float dx = end_pose_.position.x - start_pose_.position.x;
            float dy = end_pose_.position.y - start_pose_.position.y;
            float dz = 0.0;
            //float dz = end_pose_.position.z - start_pose_.position.z;

        return sqrt((float)(dx * dx + dy * dy + dz * dz));
        }




    bool PathPlanner::PlanandExecute(const geometry_msgs::PoseStamped& start_pose,const geometry_msgs::PoseStamped& end_pose){

        float dist = Get3dDistance_(start_pose.pose,end_pose.pose);
            std::cout << "dist" << dist << std::endl;
            if (dist>tolerance){
                if (not planner_called){
                    planner_called = Call_Planning_Path(start_pose,end_pose);
                    std::cout << "goal_pose_" << end_pose << std::endl;
                    return false;
                }
                else if (publishing){
                      std::cout << "planning" << exec_called << std::endl;
                      return false;
                }
                else{
                    publishing = false;
                    planner_called = false;
                    std::cout << "publish_finished" << publishing << std::endl;
                    return false;

                }
                ROS_INFO("planning and executing");
                
            }
            else{
                    publishing = false;
                    planner_called = false;
                    return true;
                    ROS_INFO("planning and executing finished");
                
            }
    }


    std::vector<float> PathPlanner::get_m_n(float x_0,float y_0,float x_1,float y_1){
        //std::cout << "x0" << x_0 << "y0" << y_0 << std::endl;
        //std::cout << "x1" << x_1 << "y1" << y_1 << std::endl;
        std::vector<float> m_n;
        Ay = y_1 - y_0;
        Ax = x_1 - x_0;
        m = (Ay)/(Ax);
        n = y_1 - m*x_1;
        m_n.push_back(m);
        m_n.push_back(n);
        return m_n;

    }




    void PathPlanner::GoToPoint(float z_1){
       
        //goal position
        goal_pose.header.frame_id = "world";
        goal_pose.pose.position.x = firefly_goal_pose.position.x;
        goal_pose.pose.position.y = firefly_goal_pose.position.y;
        goal_pose.pose.position.z = firefly_goal_pose.position.z;


        start_pose.header.frame_id = "world";
        start_pose.pose.position = firefly_init_pose.position;
        start_pose.pose.orientation = firefly_init_pose.orientation;
        
        //std::cout << "myQuaternion " << q[2] << std::endl;

        std::vector<float> m_n = get_m_n(start_pose.pose.position.x,start_pose.pose.position.y,goal_pose.pose.position.x ,goal_pose.pose.position.y);
        dist_waypoint  = Get3dDistance_(start_pose.pose,waypoint_pose.pose);
        float yaw = atan2(Ay,Ax);
        std::cout << "yaw" << yaw << std::endl;
        tf2::Quaternion q;
        q.setRPY( 0, 0, yaw );  
        if ((goal_pose.pose.position.x != 0)or(goal_pose.pose.position.y != 0)or(goal_pose.pose.position.z != 0)){
            if (init_waypoint){
                x_0 = start_pose.pose.position.x;
                y_0 = start_pose.pose.position.y;
                //x_1 = x_0 + k_x*Ax;
                //y_1 = +m*x_1 + n;
                x_1 = x_0 + 0.5;
                y_1 = y_0 + 0.5;
                if (x_1>goal_pose.pose.position.x){
                    x_1 = goal_pose.pose.position.x;
                }
                else if (y_1 > goal_pose.pose.position.y ){
                    y_1 = goal_pose.pose.position.y;
                }
                /*Ax = x_1 - x_0;
                Ay = y_1 - y_0;
                float yaw = atan2(Ay,Ax);
                std::cout << "yaw" << yaw << std::endl;
                tf2::Quaternion q;
                q.setRPY( 0, 0, yaw );  
                std::cout << "myQuaternion " << q[2] << std::endl;*/

                std::cout << "holis" << std::endl;
                waypoint_pose.header.frame_id = "world";
                waypoint_pose.pose.position.x = x_1;
                waypoint_pose.pose.position.y = y_1;    
                waypoint_pose.pose.position.z = z_1;  
                waypoint_pose.pose.orientation.z = q[2];
                waypoint_pose.pose.orientation.w = q[3];

                Eigen::Vector3d position(x_1,y_1,z_1);
                
                is_free = isCollisionFree(position);
                

                init_waypoint = false;

                if ((is_free)||((distance==0)&&(not is_free)))
                    {
                        
                        std::cout << "waypoint_pose_init_ax" << waypoint_pose << std::endl;
                        pub_pose.publish(waypoint_pose);
                        //plan_exec = PlanandExecute(start_pose,waypoint_pose);
                    }
                else{
                    //if ((not publishing) and (not plan_exec)){
                    //y_1 = y_0 + k_y*Ay;
                    /*if ( -0.2 < Ay < 0.2){
                        Ay= 0.2;
                    }*/
                    y_1 = y_0 + k_y*Ay;
                    x_1 = x_0;
                    if (x_1>goal_pose.pose.position.x){
                        x_1 = goal_pose.pose.position.x;
                    }
                    /*else if (y_1 > goal_pose.pose.position.y ){
                        y_1 = goal_pose.pose.position.y;
                    }*/

                    
                    /*float yaw = atan2(Ay,Ax);
                    std::cout << "yaw" << yaw << std::endl;
                    tf2::Quaternion q;
                    q.setRPY( 0, 0, yaw );  
                    std::cout << "myQuaternion " << q[2] << std::endl;*/


                    waypoint_pose.header.frame_id = "world";
                    waypoint_pose.pose.position.x = x_1;
                    waypoint_pose.pose.position.y = y_1;    
                    waypoint_pose.pose.position.z = z_1;  
                    waypoint_pose.pose.orientation.z = q[2];
                    waypoint_pose.pose.orientation.w = q[3];
                    pub_pose.publish(waypoint_pose);
                    
                    std::cout << "waypoint_pose_init_ay" << waypoint_pose << std::endl;
                    //plan_exec = PlanandExecute(start_pose,waypoint_pose);
                }  
                std::cout << "distance_waypoint" << dist_waypoint << std::endl;
            }
            else if (dist_waypoint < 3    ){
                    x_0 = x_1;
                    y_0 = y_1;
                    x_d = x_1 + k_x*Ax;//0.01*Ax;
                    y_d = +m*x_d + n;
                    Eigen::Vector3d position(x_d,y_d,z_1); 
                    bool is_free_ = isCollisionFree(position);
                    std::cout << "distance_waypoint" << dist_waypoint << std::endl;
                    std::cout << "distance" << distance << std::endl;
                    std::cout << "Ax" << Ax << std::endl;
                    float yaw = atan2(Ay,Ax);
                    std::cout << "yaw" << yaw << std::endl;
                    tf2::Quaternion q;
                    q.setRPY( 0, 0, yaw );  
                    std::cout << "myQuaternion " << q[2] << std::endl;
                    if ((is_free_)&&(distance<2)){

                        x_1 = x_0 + 0.3*abs(distance);
                        y_1 = y_0 + 0.2*abs(distance);
                        std::cout << "holis1" <<std::endl;
                        //waypoint_pose.pose.orientation.z = q[2];
                        //waypoint_pose.pose.orientation.w = q[3];
                        Ax = x_1 - x_0;
                        Ay = y_1 - y_0;
                         float yaw = atan2(Ay,Ax);
                        std::cout << "yaw" << yaw << std::endl;
                        tf2::Quaternion q;
                        q.setRPY( 0, 0, yaw );  
                        std::cout << "myQuaternion " << q[2] << std::endl;
                    
                    }
                    else if (((distance==0)&&(not is_free_))||(distance>=2)){

                        if (Ax > 2){
                            Ax = 2;
                        }
                
                        x_1 = x_0 + k_x*Ax;//0.01*Ax;
                        y_1 = +m*x_1 + n;
                        if (distance<(abs(abs(y_1)-abs(y_0)))<=2||(distance==0)){
                            y_1 = y_0 + distance*0.2*((Ay)/abs(Ay));
                        }
                        Eigen::Vector3d position_2(x_1,y_1,z_1); 
                        is_free_ = isCollisionFree(position_2);
                        if (not is_free_){
                            //Ay = (abs(y_1) - abs(y_0));
                            x_1 = x_0 + 0.1*distance;
                            y_1 = y_0 + 0.05*distance;
                            std::cout << "is_not_free" << std::endl;

                            Ax = x_1 - x_0;
                            Ay = y_1 - y_0;
                            float yaw = atan2(Ay,Ax);
                            std::cout << "yaw" << yaw << std::endl;
                            tf2::Quaternion q;
                            q.setRPY( 0, 0, yaw );  
                            std::cout << "myQuaternion " << q[2] << std::endl;
                        }
                        
                        Ay = (abs(y_1) - abs(y_0));
                        /*if (abs(y_1) - abs(y_0)>3){
                            y_1 = y_1 + 0.2*((Ay)/abs(Ay));   
                            std::cout << "ywetaetat" << std::endl;
                        }
                        else{
                            y_1 = +m*x_1 + n;
                        }*/
                        std::cout << "x_1" << x_1 << std::endl;
                        std::cout << "y_1" << y_1 << std::endl;
                        std::cout << "m" << m << std::endl;
                        std::cout << "n" << n << std::endl;
                        std::cout << "holis2" <<std::endl;
                        std::cout << "Ay" << Ay <<std::endl;
                        //std::cout << "waypoint_pose_ax" << waypoint_pose << std::endl;
                    }
                    else {

                        //if (Ay<0)
                        //{
                        //y_1 = y_1 + 0.1;
                        //x_1 = x_1+ 0.1;
                        if (yaw < -1.2){
                            y_1 = y_1;
                            x_1 = x_1+ 0.05*(Ax/abs(Ax))  ;
                        }
                        else{
                            y_1 = y_1+ 0.05*(Ax/abs(Ax))  ;
                            x_1 = x_1;//+ 0.05*(Ay)/abs(Ay);// + 0.05*(Ax/abs(Ax));
                        }

                        Ax = x_1 - x_0;
                        Ay = y_1 - y_0;
                        float yaw = atan2(Ay,Ax);
                        std::cout << "yaw" << yaw << std::endl;
                        tf2::Quaternion q;
                        q.setRPY( 0, 0, yaw );  
                        std::cout << "myQuaternion " << q[2] << std::endl;
                        //}
                        //y_1 = y_1 + k_y*Ay;
                        /*if (Ax == 0){
                            Ax = 1;
                        }
                        else{
                             Ax = x_1 - x_0;   
                        }
                        if (Ay == 0){
                            Ay = 1;
                        }                        
                        else{
                            Ay = y_1 - y_0;
                        }
                        float yaw = atan2(Ay,Ax);
                        std::cout << "yaw" << yaw << std::endl;
                        tf2::Quaternion q;
                        q.setRPY( 0, 0, yaw );  */

                        
                        
                        std::cout << "holis3" <<std::endl;
                       


                    }  

                   
                    //ros::Duration(0.1).sleep();
                    waypoint_pose.header.frame_id = "world";
                    waypoint_pose.pose.position.x = x_1;
                    waypoint_pose.pose.position.y = y_1;    
                    waypoint_pose.pose.position.z = z_1;  
                    waypoint_pose.pose.orientation.z = q[2];
                    waypoint_pose.pose.orientation.w = q[3];
                    
                    //MakeRotation(0.737585179684,0.675254102327);
                    //ros::Duration(0.3).sleep();
                    Eigen::Vector3d position_2(x_1,y_1,z_1); 
                    is_free_ = isCollisionFree(position_2);
                    //if (is_free){
                        pub_pose.publish(waypoint_pose);
                    //}


                   /* std::cout << "Ay" << n << std::endl;
                    std::cout << "Ax" << n << std::endl;
                    std::cout << "m" << m << std::endl;
                    std::cout << "n" << n << std::endl;
                     std::cout << "is_free" << is_free_ << std::endl;
                    std::cout << "is_free" << is_free_ << std::endl;
                    std::cout << "distance_waypoint" << dist_waypoint << std::endl;*/
                    //ros::Duration(0.1).sleep();
                  
                        
                    //plan_exec = PlanandExecute(start_pose,waypoint_pose);
                    //if ((not publishing) and (not plan_exec)){
                   
                }
               
            
            else{
                //std::cout << "publishing" << dist_waypoint << std::endl;
                 pub_pose.publish(waypoint_pose);
                //std::cout << "executing" << std::endl;
                //std::cout << "distance_collision" << distance << std::endl;
                //std::cout << "distance_waypoint" << dist_waypoint << std::endl;
                //std::cout << "is_free" << is_free << std::endl;
            }

        }
    }

     float PathPlanner::GoToPoint_(std::vector<std::vector<float>> point, float z_1){
        start_pose.header.frame_id = "world";
        start_pose.pose.position = firefly_init_pose.position;
        start_pose.pose.orientation = firefly_init_pose.orientation;
        Ax = point[0][0] - start_pose.pose.position.x;
        Ay = point[0][1] - start_pose.pose.position.y;
        float yaw = atan2(Ay,Ax);
        tf2::Quaternion q;
        q.setRPY( 0, 0, yaw ); 
        //end pose
   
        waypoint_pose.header.frame_id = "world";
        waypoint_pose.pose.position.x = point[0][0];
        waypoint_pose.pose.position.y = point[0][1]; 
        waypoint_pose.pose.position.z = z_1;  
        waypoint_pose.pose.orientation.z = q[2];
        waypoint_pose.pose.orientation.w = q[3];
        dist_waypoint  = Get3dDistance_(start_pose.pose,waypoint_pose.pose);


        if (dist_waypoint > 2){
                    pub_pose.publish(waypoint_pose);
                }
        //std::cout << "dist_waypoint: " << dist_waypoint << std::endl;
        return dist_waypoint;
     }

     void PathPlanner::PublishGoal(const geometry_msgs::Pose& pose){
           goal_pose.header.frame_id = "world";
           goal_pose.pose.position = pose.position;
           goal_pose.pose.orientation = pose.orientation;

        pub_pose.publish(goal_pose); 
     } 


     /*bool PathPlanner::GoToPoint_(std::string name, float z_1){
         int size = 0;
         int i=0;
         std::vector<std::vector<float> > b;
         std::cout << "name" << std::endl;
         if (name == "SHELVE"){
              std::vector<std::vector<float> > a{{7.4,10}, {15,17.4}, {32.1,17.6}, {24.9,5},{22.2,4.9}};
              b=a;
              size = sizeof(a);
              i=0;
              std::cout << "holis" << std::endl;

         }
            
            while (i<size){
                //std::cout << "b" << b[-1][-1] << std::endl;
                //start pose 
                start_pose.header.frame_id = "world";
                start_pose.pose.position = firefly_init_pose.position;
                start_pose.pose.orientation = firefly_init_pose.orientation;
                Ax = b[i][0] - start_pose.pose.position.x;
                Ay = b[i][1] - start_pose.pose.position.y;
                float yaw = atan2(Ay,Ax);
               
                tf2::Quaternion q;
                q.setRPY( 0, 0, yaw ); 
                //end pose
                std::cout << "i: " << i << std::endl;
                waypoint_pose.header.frame_id = "world";
                waypoint_pose.pose.position.x = b[i][0];
                waypoint_pose.pose.position.y = b[i][1]; 
                waypoint_pose.pose.position.z = z_1;  
                waypoint_pose.pose.orientation.z = q[2];
                waypoint_pose.pose.orientation.w = q[3];
                std::cout 

                dist_waypoint  = Get3dDistance_(firefly_init_pose,waypoint_pose.pose);
                std::cout << "firefly_init_pose: " << firefly_init_pose << std::endl;
                std::cout << "start_pose: " << start_pose << std::endl;
                //std::cout << "waypoint_pose: " << waypoint_pose << std::endl;
                if (dist_waypoint > 2){
                    pub_pose.publish(waypoint_pose);
                }
                else{
                    i+=1;
                }

                goal_pose.header.frame_id = "world";
                goal_pose.pose.position.x = b[-1][-1];
                goal_pose.pose.position.y = b[-1][-1];
                goal_pose.pose.position.z = z_1;

                float dist_goal  = Get3dDistance_(start_pose.pose,goal_pose.pose);
                if (dist_goal < 1){
                    return true;
                }         
                           




              }

         

     }*/


} // namespace voxblox
