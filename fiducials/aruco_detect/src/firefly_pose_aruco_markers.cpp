
#include <ros/ros.h>
#include "fiducial_msgs/Fiducial.h"
#include "fiducial_msgs/FiducialArray.h"
#include "fiducial_msgs/FiducialTransform.h"
#include "fiducial_msgs/FiducialTransformArray.h"
//#include "aruco_detect/DetectorParamsConfig.h"
#include "geometry_msgs/PointStamped.h"
#include "geometry_msgs/Transform.h"
#include "geometry_msgs/Vector3.h"
#include "Eigen/Eigen"

#include "std_msgs/Bool.h" 

#include <assert.h>
#include <sys/time.h>
#include <unistd.h>

#include <ros/ros.h>
#include <tf/transform_datatypes.h>
#include <tf2/LinearMath/Transform.h>
#include <tf2_ros/buffer.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <visualization_msgs/Marker.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include <dynamic_reconfigure/server.h>
#include <std_srvs/SetBool.h>
#include <std_msgs/String.h>

#include "fiducial_msgs/Fiducial.h"
#include "fiducial_msgs/FiducialArray.h"
#include "fiducial_msgs/FiducialTransform.h"
#include "fiducial_msgs/FiducialTransformArray.h"
//#include "aruco_detect/DetectorParamsConfig.h"

#include <opencv2/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/calib3d.hpp>

#include <list>
#include <boost/algorithm/string.hpp>

#include <tf/transform_listener.h>

using namespace std;
using namespace cv;


ros::Publisher * pose_world_pub;
ros::Publisher * marker_world_pub;
ros::Publisher * husky_world_pub;
geometry_msgs::PoseStamped pose_drone_estimated;

//tf::Transform camera_T_drone;

geometry_msgs::PoseStamped read_param_global_markers(int id)
{    
    std::vector<int> idList;
    std::vector<double> poseList;

  //  ROS_INFO("Global markers: ");

    /*
        /groundMarkers/idList
        /groundMarkers/nameList
        /groundMarkers/numberOfMarkers
        /groundMarkers/orientationFormat
        /groundMarkers/poseList
    */

    if(!ros::param::get("//groundMarkers/idList", idList))
        ROS_ERROR("Failed to read idList on param server");
    //std::cout << "idList  " << idList[0] << std::endl;

    if(!ros::param::get("//groundMarkers/poseList", poseList))
        ROS_ERROR("Failed to read poseList on param server");
    //std::cout << "poseList  " << poseList[0] << std::endl;

  //  ROS_INFO("Get pose marker");

    geometry_msgs::PoseStamped markerposition ;

    markerposition.pose.position.x = poseList[id*6];
    markerposition.pose.position.y = poseList[id*6+1];
    markerposition.pose.position.z = poseList[id*6+2];
    markerposition.pose.orientation.x = poseList[id*6+3];
    markerposition.pose.orientation.y = poseList[id*6+4];
    markerposition.pose.orientation.z = poseList[id*6+5];
// Transformar orientació a quaterniooo


   // std::cout << "Marker position" << markerposition << std::endl;

    return markerposition;

}

geometry_msgs::PoseWithCovarianceStamped getPoseInWorld(int id, geometry_msgs::Transform transform_aruco){

//world_T_drone = world_T_marker*marker_T_camera*camera_T_drone

      tf::Transform marker_T_camera;
      tf::transformMsgToTF(transform_aruco, marker_T_camera);
      marker_T_camera = marker_T_camera.inverse();

      geometry_msgs::PoseStamped pose_marker;
      pose_marker = read_param_global_markers(id);

      tf::Transform world_T_marker;
      world_T_marker.setOrigin(tf::Vector3(pose_marker.pose.position.x,pose_marker.pose.position.y,pose_marker.pose.position.z));
      world_T_marker.setRotation(tf::Quaternion(pose_marker.pose.orientation.x, pose_marker.pose.orientation.y, pose_marker.pose.orientation.z, 1));

      tf::Transform camera_T_drone;
      camera_T_drone.setOrigin(tf::Vector3(0,0,-0.02));
      camera_T_drone.setRotation(tf::Quaternion(0.707,-0.707,0,0));

      tf::Transform transform_drone_world;
      transform_drone_world = world_T_marker*marker_T_camera*camera_T_drone;
	    geometry_msgs::Transform test;
      tf::transformTFToMsg(transform_drone_world, test);

      geometry_msgs::PoseWithCovarianceStamped drone_pose;
      drone_pose.pose.pose.position.x = transform_drone_world.getOrigin().x();
      drone_pose.pose.pose.position.y = transform_drone_world.getOrigin().y();
      drone_pose.pose.pose.position.z = transform_drone_world.getOrigin().z();
      drone_pose.pose.pose.orientation.x =  transform_drone_world.getRotation().x();
      drone_pose.pose.pose.orientation.y =  transform_drone_world.getRotation().y();
      drone_pose.pose.pose.orientation.z =  transform_drone_world.getRotation().z();
      drone_pose.pose.pose.orientation.w =  transform_drone_world.getRotation().w();
      drone_pose.header.frame_id = "world";
      drone_pose.header.stamp = ros::Time::now(); 
     // std::cout << "Drone posició  "<<drone_pose.pose << std::endl;
     

     return drone_pose;


}

geometry_msgs::PoseStamped getMarkInWorld(int id, geometry_msgs::Transform transform_aruco){

//world_T_marker = world_T_drone*drone_T_camera*camera_T_marker;

      tf::Transform world_T_drone;
      world_T_drone.setOrigin(tf::Vector3(pose_drone_estimated.pose.position.x, pose_drone_estimated.pose.position.y, pose_drone_estimated.pose.position.z));
      world_T_drone.setRotation(tf::Quaternion(pose_drone_estimated.pose.orientation.x, pose_drone_estimated.pose.orientation.y, pose_drone_estimated.pose.orientation.z,pose_drone_estimated.pose.orientation.w));

      tf::Transform camera_T_marker;
      tf::transformMsgToTF(transform_aruco, camera_T_marker);

      tf::Transform drone_T_camera;
      drone_T_camera.setOrigin(tf::Vector3(0,0,-0.02));
      drone_T_camera.setRotation(tf::Quaternion(0.707,-0.707,0,0));
    
      tf::Transform world_T_marker;
      world_T_marker = world_T_drone*camera_T_marker;//drone_T_camera*camera_T_marker;

	    geometry_msgs::Transform test;
      tf::transformTFToMsg(world_T_marker, test);

      geometry_msgs::PoseStamped marker_pose;
      marker_pose.pose.position.x = world_T_marker.getOrigin().x();
      marker_pose.pose.position.y = world_T_marker.getOrigin().y();
      marker_pose.pose.position.z = world_T_marker.getOrigin().z();
      marker_pose.pose.orientation.x =  world_T_marker.getRotation().x();
      marker_pose.pose.orientation.y =  world_T_marker.getRotation().y();
      marker_pose.pose.orientation.z =  world_T_marker.getRotation().z();
      marker_pose.pose.orientation.w =  world_T_marker.getRotation().w();
      marker_pose.header.frame_id = std::to_string(id);
      marker_pose.header.stamp = ros::Time::now();

   //   std::cout << "Mark posició  "<<marker_pose << std::endl;
     
     return marker_pose;

}


void transforms_Callback(fiducial_msgs::FiducialTransformArray fta)

{   //ROS_INFO("Callback transforms");

if(fta.transforms.size()>0){

    int id;
    
    fiducial_msgs::FiducialTransform transform;

    transform.transform = fta.transforms[0].transform;

    id =  fta.transforms[0].fiducial_id;

    geometry_msgs::PoseWithCovarianceStamped point;


    point = getPoseInWorld(id, transform.transform);

  if (point.pose.pose.position.z > 1.2 ){
      pose_world_pub -> publish(point);
       }
    }

}

void transforms_callback_frontal(fiducial_msgs::FiducialTransformArray fta){
	//ROS_INFO("Callback transforms fronal");

	if(fta.transforms.size()>0){
	
		int id;

		fiducial_msgs::FiducialTransform transform;

    	transform.transform = fta.transforms[0].transform;

    	id =  fta.transforms[0].fiducial_id;

    	geometry_msgs::PoseStamped point;

    	point = getMarkInWorld(id, transform.transform);


    	marker_world_pub -> publish(point);

	}

}


void transforms_callback_husky(fiducial_msgs::FiducialTransformArray fta){
 // ROS_INFO("Callback transforms fronal");

  if(fta.transforms.size()>0){
  
    int id;

    fiducial_msgs::FiducialTransform transform;

      transform.transform = fta.transforms[0].transform;

      id =  fta.transforms[0].fiducial_id;

      geometry_msgs::PoseStamped point;

      point = getMarkInWorld(id, transform.transform);


      marker_world_pub -> publish(point);

  }

}


void transforms_callback_pose_drone(geometry_msgs::Pose pose){

	pose_drone_estimated.pose = pose;
}


int main(int argc, char ** argv) {
   
    ros::init(argc, argv, "Pose_aruco_markers");
    ros::NodeHandle nh;

    ros::Subscriber sub_transf = nh.subscribe("/firefly/camera_ventral/fiducial_transforms", 1, transforms_Callback);
    pose_world_pub = new ros::Publisher(nh.advertise<geometry_msgs::PoseWithCovarianceStamped>("/firefly/camera_ventral/estimated_drone_point", 1));

    ros::Subscriber sub_transf_frontal = nh.subscribe("/firefly/vi_sensor/camera_depth/camera/fiducial_transforms",1, transforms_callback_frontal);
    marker_world_pub = new ros::Publisher(nh.advertise<geometry_msgs::PoseStamped>("/firefly/vi_sensor/camera_depth/estimated_marker_point", 1));

    ros::Subscriber sub_transf_husky = nh.subscribe("/husky/fiducial_transforms",1, transforms_callback_husky);
    husky_world_pub = new ros::Publisher(nh.advertise<geometry_msgs::PoseStamped>("/husky/estimated_marker_point", 1));

	  ros::Subscriber sub_pose_drone = nh.subscribe("/firefly/vi_sensor/ground_truth/pose",1, transforms_callback_pose_drone);

    ros::spin();

    return 0;
}
