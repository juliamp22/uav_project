#include "ros/ros.h"
#include "sensor_msgs/Imu.h"
#include "geometry_msgs/TwistWithCovarianceStamped.h"
#include <tf/transform_broadcaster.h>

sensor_msgs::Imu imu_out_1;
sensor_msgs::Imu imu_out_2;

ros::Publisher imu_pub_1;
ros::Subscriber imu_sub_1;

ros::Publisher imu_pub_2;
ros::Subscriber imu_sub_2;

  void imu_callback(const sensor_msgs::Imu::ConstPtr& msg)
  {
    imu_out_1 = *msg;
    imu_out_1.header.frame_id = "firefly/imu_link2";

    imu_pub_1.publish(imu_out_1);

    }

  void vi_imu_callback(const sensor_msgs::Imu::ConstPtr& msg)
  {
    imu_out_2 = *msg;
    imu_out_2.header.frame_id = "firefly/vi_sensor/imu_link2";

    imu_pub_2.publish(imu_out_2);

    }




int main(int argc, char** argv)
{
  ros::init(argc, argv, "sensor_cloner");
  ros::NodeHandle nh_;
  /*tf::TransformBroadcaster br;
  tf::Transform transform;*/


  imu_pub_1 = nh_.advertise<sensor_msgs::Imu>("/firefly/imu2", 10, false);
  imu_sub_1 = nh_.subscribe("/firefly/imu", 1000, imu_callback);

  imu_pub_2 = nh_.advertise<sensor_msgs::Imu>("/firefly/vi_sensor/imu2", 10, false);
  imu_sub_2 = nh_.subscribe("/firefly/vi_sensor/imu", 1000, vi_imu_callback);

  /*transform.setOrigin( tf::Vector3(0.0, 0.0, 0.0) );
  transform.setRotation( tf::Quaternion(0, 0, 0, 1) );
  br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "firefly/base_link2", "firefly/imu_link2"));*/

  ros::spin();
}
