
#include <ros/ros.h>


void read_param_global_markers()
{
    std::vector<int> idList;
    std::vector<double> poseList;
    
    ROS_INFO("Global markers: ");

    /*
        /groundMarkers/idList
        /groundMarkers/nameList
        /groundMarkers/numberOfMarkers
        /groundMarkers/orientationFormat
        /groundMarkers/poseList
    */

if(!ros::param::get("//groundMarkers/idList", idList))
    ROS_ERROR("Failed to read idList on param server");
//std::cout << "idList  " << idList[0] << std::endl;

if(!ros::param::get("//groundMarkers/poseList", poseList))
    ROS_ERROR("Failed to read poseList on param server");
//std::cout << "poseList  " << poseList[0] << std::endl;

}

void transforms_Callback(fiducial_msgs::FiducialTransformArray fta)

{   ROS_INFO("Callback transforms");


}

int main(int argc, char ** argv) {
   
    ros::init(argc, argv, "Pose_aruco_markers");
    ros::NodeHandle nh;

    read_param_global_markers();

    ros::Subscriber sub_transf = nh.subscribe("/fiducial_transforms", 1, transforms_Callback);
    

    ros::spin();

    return 0;
}
