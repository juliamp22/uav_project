#include <ros/ros.h>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Vector3.h"
#include "mav_planning_msgs/PlannerService.h"
#include "std_srvs/Empty.h"
#include "chrono"
#include<iostream>

ros::ServiceClient srv_planning_path;
mav_planning_msgs::PlannerService srv_planner;

ros::ServiceClient srv_publish_path;
std_srvs::Empty srv_publisher;

geometry_msgs::Pose firefly_pose;

float tolerance = 0.4;
bool publishing = false;
float dist=0;

float Get3dDistance( std::vector<double>  c1, std::vector<double>  c2 )
{
    float dx = c2[0] - c1[0];
    float dy = c2[1] - c1[1];
    float dz = c2[2] - c1[2];

return sqrt((float)(dx * dx + dy * dy + dz * dz));
}


void callPlanningService(geometry_msgs::PoseStamped start_pose, geometry_msgs::PoseStamped goal_pose){

 /*geometry_msgs/PoseStamped start_pose #start pose for the planner
geometry_msgs/Vector3 start_velocity
geometry_msgs/PoseStamped goal_pose #start pose for the planner
geometry_msgs/Vector3 goal_velocity
geometry_msgs/Vector3 bounding_box*/  

/*IMPORTANT, FALTARIA DEFINIR LES VELOCITATAS DELS GOALS INTERMITJOS!!!!*/             

// Call al servicio de planner
    srv_planner.request.start_pose = start_pose;
    srv_planner.request.goal_pose = goal_pose;
    srv_planner.request.bounding_box.x = 10;
    srv_planner.request.bounding_box.y = 10;
    srv_planner.request.bounding_box.z = 10;

    try{
        srv_planning_path.call(srv_planner);
    }
    catch(...){}


//Miramos a ver si planner success= true con 5 segundos de timeout
    auto start = std::chrono::system_clock::now();
    auto end = std::chrono::system_clock::now();
    while((srv_planner.response.success == false)and(std::chrono::duration_cast<std::chrono::seconds>(end - start).count() != 5)){

        end = std::chrono::system_clock::now();   
    }
  

   if (srv_planner.response.success == false)
    {  
        ROS_ERROR("Ha fallado al planear");
     } 
    else{
    
    //si se ha podido planear hacemos publish de los puntos. 
        srv_publish_path.call(srv_publisher);
        publishing = true;

       }


}

void mission(){
   // ROS_INFO("Start mission");

    geometry_msgs::PoseStamped start_pose, goal_pose;

    start_pose.header.frame_id = "world";
    start_pose.pose.position.x = firefly_pose.position.x;
    start_pose.pose.position.y = firefly_pose.position.y;
    start_pose.pose.position.z = firefly_pose.position.z;

    goal_pose.header.frame_id = "world";
    goal_pose.pose.position.x = -2.5;
    goal_pose.pose.position.y = -2.5;
    goal_pose.pose.position.z = 1.0;

//Si no se está publicando otro goal ejecutamos servicio 
    if ((publishing==false)and dist>abs(tolerance)){
    callPlanningService(start_pose, goal_pose);
    }
    else{
        if(dist< abs(tolerance)and (publishing==true)){
         publishing=false;
         ROS_INFO("firefly ha llegado al goal");
        }
    }

//para comprobar la distancia del firefly con el goal final
    std::vector<double> c1,c2;
    c1 = {goal_pose.pose.position.x, goal_pose.pose.position.y, goal_pose.pose.position.z};
    c2 = {firefly_pose.position.x, firefly_pose.position.y, firefly_pose.position.z};
    dist = Get3dDistance(c1,c2);
   // std::cout << "Pose::  "<<firefly_pose<< std::endl;
}

void firefly_pose_callback(geometry_msgs::Pose pose){

    firefly_pose = pose;
}



int main(int argc, char **argv)
{
    ros::init(argc, argv, "mission_node");
    ros::NodeHandle nh("");

//Init params
    nh.setParam("/firefly/voxblox_node/clear_sphere_for_planning", false);
    nh.setParam("/firefly/voxblox_node/clear_sphere_radius", 10);
    nh.setParam("/firefly/voxblox_node/occupied_sphere_radius", 0);

    ros::Subscriber sub_persones = nh.subscribe ("/firefly/vi_sensor/ground_truth/pose", 1, firefly_pose_callback);
   
//fer topic feedback final

//creamos los service client
    ros::service::waitForService("/firefly/voxblox_rrt_planner/plan", -1);
    ros::service::waitForService("/firefly/voxblox_rrt_planner/publish_path", -1);

    srv_planning_path = nh.serviceClient<mav_planning_msgs::PlannerService>("/firefly/voxblox_rrt_planner/plan");
    srv_publish_path = nh.serviceClient<std_srvs::Empty>("/firefly/voxblox_rrt_planner/publish_path");

    ROS_INFO("Running mission node...");

    mission();

        ros::Rate r(5);
    while(ros::ok()){
        ros::spinOnce();
        mission();  
        r.sleep();
        }


    return 0;
}